﻿using InterfacesAndTheirFeatures.MocByAddAcount.Interfaces;
using System;

namespace InterfacesAndTheirFeatures.MocByAddAcount.Models
{
    public class AccountUser : IAccount
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
    }
}
