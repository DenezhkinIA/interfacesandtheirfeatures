﻿using InterfacesAndTheirFeatures.MocByAddAcount.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InterfacesAndTheirFeatures.MocByAddAcount.Interfaces
{
    interface IAccountService
    {
        public void AddAccount(AccountUser account);
    }
}
