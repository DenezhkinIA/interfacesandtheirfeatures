﻿using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;

namespace InterfacesAndTheirFeatures.MocByAddAcount.Interfaces
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        T GetOne(Func<T, bool> predicate);
        void Add(T item);
    }
}
