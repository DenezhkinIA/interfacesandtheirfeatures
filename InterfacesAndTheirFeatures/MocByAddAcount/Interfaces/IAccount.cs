﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfacesAndTheirFeatures.MocByAddAcount.Interfaces
{
    public interface IAccount
    {
        public string FirstName { get;}
        public string LastName { get;}
        public DateTime? BirthDate { get;}
    }
}
