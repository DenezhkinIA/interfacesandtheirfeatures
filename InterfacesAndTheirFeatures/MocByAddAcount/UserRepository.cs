﻿using InterfacesAndTheirFeatures.MocByAddAcount.Interfaces;
using InterfacesAndTheirFeatures.MocByAddAcount.Models;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InterfacesAndTheirFeatures.MocByAddAcount
{

    public class UserRepository : IRepository<AccountUser>
    {
        private List<AccountUser> accountList;

        public UserRepository()
        {
            accountList = new List<AccountUser>();
            accountList.Add(new AccountUser { FirstName = "Костя", LastName = "Бобров" });
            accountList.Add(new AccountUser { FirstName = "Вася", LastName = "Петров", BirthDate = new DateTime(1988, 06, 07) });
            accountList.Add(new AccountUser { FirstName = "Гадя", LastName = "Меч", BirthDate = new DateTime(1981, 10, 11) });
            accountList.Add(new AccountUser { FirstName = "Полина", LastName = "Енисеева", BirthDate = new DateTime(1993, 01, 03) });
            accountList.Add(new AccountUser { FirstName = "Фахид", LastName = "Оритов" });
        }
        public UserRepository(List<AccountUser> list)
        {
            accountList = list;
        }

        public void Add(AccountUser item)
        {
            accountList.Add(item);
        }

        public IEnumerable<AccountUser> GetAll()
        {
            foreach (var item in accountList)
            {
                yield return item;
            }
        }

        public AccountUser GetOne(Func<AccountUser, bool> predicate)
        {
            return accountList.SingleOrDefault(predicate);
        }

    }
}
