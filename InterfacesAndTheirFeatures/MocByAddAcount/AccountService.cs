﻿using InterfacesAndTheirFeatures.MocByAddAcount.Interfaces;
using InterfacesAndTheirFeatures.MocByAddAcount.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace InterfacesAndTheirFeatures.MocByAddAcount
{
    public class AccountService : IAccountService
    {
        private IRepository<AccountUser> _rep;
        public AccountService(IRepository<AccountUser> rep)
        {
            _rep = rep;
        }
        public void AddAccount(AccountUser account)
        {
            if (string.IsNullOrWhiteSpace(account.FirstName))
                throw new NullReferenceException("Введите имя");
            if (string.IsNullOrWhiteSpace(account.LastName))
                throw new NullReferenceException("Введите фамилию");
            _rep.Add(account);
        }
    }

}
