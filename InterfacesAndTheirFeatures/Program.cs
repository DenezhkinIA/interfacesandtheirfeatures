﻿using System;

namespace InterfacesAndTheirFeatures
{
    class Program
    {

        static void Main(string[] args)
        {
            HomeWorkInterfaces homeWork = new HomeWorkInterfaces(new Settings(AppContext.BaseDirectory, "appsettings.json"));
            homeWork.Run();

        }
    }
}
