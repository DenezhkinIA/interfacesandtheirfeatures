﻿
namespace InterfacesAndTheirFeatures.Interfaces
{
    interface ISettings
    {
        string XmlFilePath { get; }
        string JsonFilePath { get; }
    }
}
