﻿using System.IO;

namespace InterfacesAndTheirFeatures.Interfaces
{
    public interface ISerializer<T>
    {
        string Serialize<T>(T item);
        T Deserialize<T>(Stream stream);
    }
}