﻿using System.Collections.Generic;

namespace InterfacesAndTheirFeatures.Interfaces
{
    interface ISorter<T>
    {
        IEnumerable<T> GetSort(IEnumerable<T> notSortedItems);
    }
}
