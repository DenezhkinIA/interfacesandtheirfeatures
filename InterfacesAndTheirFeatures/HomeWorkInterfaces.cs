﻿using System;
using System.Collections.Generic;
using System.IO;
using InterfacesAndTheirFeatures.Interfaces;
using InterfacesAndTheirFeatures.Models;
using static InterfacesAndTheirFeatures.Serealizers;

namespace InterfacesAndTheirFeatures
{
    sealed class HomeWorkInterfaces
    {
        private readonly ISettings _settings;
        private readonly string _xmlFilePath;
        private readonly string _jsonFilePath;
        public HomeWorkInterfaces(ISettings settings)
        {
            _settings = settings;
            _xmlFilePath = settings.XmlFilePath;
            _jsonFilePath = settings.JsonFilePath;

        }

        public void HWRun()
        {
            do
            {
                Console.Clear();
                ConsoleKeyInfo input;
                Console.WriteLine("Работа с файлвми:");
                List<Person> deserializeList = new List<Person>();
                bool choice = false;
                Console.WriteLine("Считать XML:X / Считать Json:J");

                while (choice == false)
                {
                    input = Console.ReadKey();
                    switch (input.Key)
                    {
                        case ConsoleKey.X:
                            {
                                deserializeList = RunDeserealiserXML(_xmlFilePath);
                                choice = true;
                                break;

                            }
                        case ConsoleKey.J:
                            {
                                deserializeList = RunDeserealiserXML(_jsonFilePath);
                                choice = true;
                                break;
                            }
                        default:
                            Console.Clear();
                            Console.WriteLine("Операция не распознана. Введите x  или J:");

                            break;
                    }
                }
                Console.Write("Начать сортировку ? Y / N");
                input = Console.ReadKey();
                switch (input.Key)
                {
                    case ConsoleKey.N:
                        {
                            break;
                        }
                    case ConsoleKey.Y:
                        {
                            deserializeList = RunDeserealiserXML(_xmlFilePath);
                            break;
                        }
                    default:
                        Console.WriteLine("Не известная команад.");
                        HWRun();
                        break;
                }
                Console.Write("Серелизовать XML:X / Серелизовать Json:J");
                input = Console.ReadKey();
                switch (input.Key)
                {
                    case ConsoleKey.X:
                        {
                            var ser = new MyXmlSerializer<List<Person>>();
                            Console.WriteLine(ser.Serialize<List<Person>>(deserializeList));
                            break;

                        }
                    case ConsoleKey.J:
                        {
                            var ser = new MyJsonSerializer<List<Person>>();
                            Console.WriteLine(ser.Serialize<List<Person>>(deserializeList));
                            break;
                        }
                    default:
                        Console.WriteLine("Не известная команад.");
                        HWRun();
                        break;
                }
            }
            while (AskContinue());
        }
        public void Run()
        {
            HWRun();
        }
        static bool AskContinue()
        {
            while (true)
            {
                Console.WriteLine("Начать заново? Y/N");
                var status = Console.ReadLine().ToUpper();
                if (status == "Y")
                    return true;
                else if (status == "N")
                    return false;
                else
                    Console.WriteLine("Операция не распознана. Введите Y или N: ");

            }
        }
        #region Десерелизаторы
        private List<Person> RunDeserealiserXML(string readFile)
        {
            List<Person> listPersone = new List<Person>();
            var file = new FileStream(readFile, FileMode.Open);
            using (var streamReader = new MyStreamReader<Person>(file, new MyXmlSerializer<Person>()))
            {

                for (int i = 0; i < streamReader.Count; i++)
                {
                    Console.WriteLine($"ID: {streamReader[i].ID}");
                    Console.WriteLine($"Имя: {streamReader[i].Name}");
                    Console.WriteLine($"Фамилия: {streamReader[i].Surname}");
                    Console.WriteLine($"Возраст: {streamReader[i].Age}");
                    Console.WriteLine();
                    listPersone.Add(streamReader[i]);
                }
            }
            return listPersone;
        }

        private List<Person> RunDeserealiserJson(string readFile)
        {
            List<Person> listPersone = new List<Person>();
            var file = new FileStream(readFile, FileMode.Open);
            using (var streamReader = new MyStreamReader<Person>(file, new MyJsonSerializer<Person>()))
            {

                for (int i = 0; i < streamReader.Count; i++)
                {
                    Console.WriteLine($"ID: {streamReader[i].ID}");
                    Console.WriteLine($"Имя: {streamReader[i].Name}");
                    Console.WriteLine($"Фамилия: {streamReader[i].Surname}");
                    Console.WriteLine($"Возраст: {streamReader[i].Age}");
                    Console.WriteLine();
                    listPersone.Add(streamReader[i]);
                }
            }
            return listPersone;
        }
        #endregion
        #region Сортировка
        private List<Person> Sorter(List<Person> deserializeList)
        {
            Console.Clear();
            List<Person> listPersone = new List<Person>();
            PersonSorter personSorter = new PersonSorter();
            var algPerson = personSorter.GetSort(deserializeList);
            foreach (var alg in algPerson)
            {
                Console.WriteLine($"ID: {alg.ID}");
                Console.WriteLine($"Имя: {alg.Name}");
                Console.WriteLine($"Фамилия: {alg.Surname}");
                Console.WriteLine($"Возраст: {alg.Age}");
                Console.WriteLine();
            }
            Console.WriteLine("Отсоритировал");
            return listPersone;
        }
        #endregion
    }
}