﻿using System.Collections.Generic;
using System.Linq;
using InterfacesAndTheirFeatures.Interfaces;
using InterfacesAndTheirFeatures.Models;

namespace InterfacesAndTheirFeatures
{
    public class PersonSorter : ISorter<Person>
    {
        public IEnumerable<Person> GetSort(IEnumerable<Person> notSortedItems)
        {
            return notSortedItems.OrderBy(x => x.Name);
        }
        public IEnumerable<Person> GetSortByIComparable(IEnumerable<Person> notSortedItems)
        {
            List<Person> person = (List<Person>)notSortedItems;
            person.Sort();
            return person;
        }
    }
}
