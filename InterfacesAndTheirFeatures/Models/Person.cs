﻿using System;

namespace InterfacesAndTheirFeatures.Models
{
    public class Person : IComparable<Person>
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public int CompareTo(Person item)
        {
            return string.Compare(this.Name, item.Name);
        }
    }
}
