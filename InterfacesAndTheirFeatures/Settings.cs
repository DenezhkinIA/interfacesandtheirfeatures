﻿using InterfacesAndTheirFeatures.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace InterfacesAndTheirFeatures
{
    sealed class Settings : ISettings
    {
        public string XmlFilePath { get; private set; }
        public string JsonFilePath { get; private set; }
        public Settings(string basePath, string file)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile(file);

            IConfiguration config = builder.Build();
            XmlFilePath = config[nameof(XmlFilePath)];
            JsonFilePath = config[nameof(JsonFilePath)];
           
        }
    }
}
