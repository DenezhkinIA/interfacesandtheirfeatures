﻿using InterfacesAndTheirFeatures.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace InterfacesAndTheirFeatures
{
    class MyStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private Stream stream;
        private ISerializer<T> serializer;
        List<T> deserialize = new List<T>();
        public MyStreamReader(Stream stream, ISerializer<T> serializer)
        {
            this.stream = stream;
            this.serializer = serializer;
            deserialize = serializer.Deserialize<List<T>>(stream);
        }

        public T this[int index]
        {
            get { return deserialize[index]; }
        }
        public void Dispose()
        {
            stream.Dispose();
        }

        public int Count
        {
            get { return deserialize.Count; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            //deserialize.GetEnumerator();
            for (int i = 0; i < deserialize.Count; i++)
            {
                yield return deserialize[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

    }
}
