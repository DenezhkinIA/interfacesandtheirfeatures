﻿using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using Newtonsoft.Json;
using InterfacesAndTheirFeatures.Interfaces;
using System.IO;
using System.Xml.Serialization;

namespace InterfacesAndTheirFeatures
{
    class Serealizers
    {

        public class MyXmlSerializer<T> : ISerializer<T>
        {
            private static readonly XmlWriterSettings XmlWriterSettings;

            static MyXmlSerializer()
            {
                XmlWriterSettings = new XmlWriterSettings { Indent = true };
            }

            public string Serialize<T>(T item)
            {
                IExtendedXmlSerializer serializer = new ConfigurationContainer()
                  .UseAutoFormatting()
                  .UseOptimizedNamespaces()
                  .EnableImplicitTyping(typeof(T))
                  .Create();

                return serializer.Serialize(XmlWriterSettings, item);
            }
            public T Deserialize<T>(Stream stream)
            {
                T arrayT;

                if (stream.Position > 0)
                {
                    stream.Position = 0;
                }
                XmlRootAttribute xRoot = new XmlRootAttribute();
                xRoot.ElementName = "List";
                xRoot.IsNullable = true;
                XmlSerializer formatter = new XmlSerializer(typeof(T), new XmlRootAttribute("List"));

                var retVal = (T)formatter.Deserialize(stream);
                return retVal;
            }


        }
        public class MyJsonSerializer<T> : ISerializer<T>
        {
            public T Deserialize<T>(Stream stream)
            {
                using (StreamReader reader = new StreamReader(stream))
                using (JsonTextReader jsonReader = new JsonTextReader(reader))
                {
                    JsonSerializer ser = new JsonSerializer();
                    var retVal = ser.Deserialize<T>(jsonReader);
                    return retVal;
                }
            }

            public string Serialize<T>( T item)
            {
                return JsonConvert.SerializeObject(item);
            }
        }
    }
}
