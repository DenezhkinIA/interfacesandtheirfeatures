using InterfacesAndTheirFeatures.MocByAddAcount;
using InterfacesAndTheirFeatures.MocByAddAcount.Interfaces;
using InterfacesAndTheirFeatures.MocByAddAcount.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NUnitTest
{
    public class Tests
    {
        private readonly AccountUser _users = new AccountUser { BirthDate = DateTime.MinValue, FirstName = "���", LastName ="�������" };
        private Mock<IRepository<AccountUser>> _mRepo;
        NullReferenceException a = new NullReferenceException("������� ���");

        [SetUp]
        public void Setup1()
        {

            _mRepo = new Mock<IRepository<AccountUser>>();
            _mRepo.Setup(x => x.Add(_users));
        }


        [Test]
        public void AccountServiceAddUserCorrectlyTest()
        {
            var account = new AccountUser { BirthDate = DateTime.MinValue, FirstName = "���", LastName = "�������" };
            var service = new AccountService(_mRepo.Object);
            service.AddAccount(account);
        }
        [Test]
        public void AccountServiceAddUserNullName()
        {
            var account = new AccountUser { BirthDate = DateTime.MinValue, FirstName = null, LastName = "������� " };
            var service = new AccountService(_mRepo.Object);
            Assert.Throws<NullReferenceException>(()=>service.AddAccount(account));
        }
        [Test]
        public void AccountServiceAddUserNullSurname()
        {
            var account = new AccountUser { BirthDate = DateTime.MinValue, FirstName = $"��� 0", LastName = null };
            var service = new AccountService(_mRepo.Object);
            Assert.Throws<NullReferenceException>(() => service.AddAccount(account));
        }

    }
}